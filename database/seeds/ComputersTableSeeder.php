<?php

use Illuminate\Database\Seeder;

class ComputersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Computer::create([
            "brand" => "HP",
            "model" => "15-bs0xx",
            "date" => "2018-01-27"
        ]);

        \App\Models\Computer::create([
            "brand" => "HP",
            "model" => "360xEnvy",
            "date" => "2019-06-15"
        ]);
    }
}
